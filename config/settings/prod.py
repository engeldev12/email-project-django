from .base import *

DEBUG = False

ALLOWED_HOSTS = [

]

EMAIL_PORT = ''
EMAIL_HOST = ''
EMAIL_HOST_USER = ''
EMAIL_HOST_PASSWORD = ''

EMAIL_BACKEND = ''

DATABASES = {
	'default': {
		'ENGINE': '',
		'NAME': '',
	}
}

STATIC_ROOT = root('sendemail/static')

STATICFILES_STORAGE = ''
