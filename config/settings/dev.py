from .base import *


DEBUG = True


EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'


DATABASES = {
	'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': root('sendemail/db.sqlite3'),
    }
}


# Configuración de celery

CELERY_BROKER_URL = 'redis://localhost:6379/0'

CELERY_TASK_ALWAYS_EAGER = False

# =============================

STATICFILES_DIRS = [root('sendemail/static')]