run:
	@python3 manage.py runserver

run_flower:
	@celery -A config.settings flower

run_celery:
	@celery -A config.settings worker -l info

migrate:
	@python3 manage.py makemigrations
	@python3 manage.py migrate

test:
	@python3 manage.py test

coverage:
	@coverage run manage.py test
	@coverage report

test_ci:
	@python manage.py test --settings=config.settings.ci

coverage_ci:
	@coverage run manage.py test --settings=config.settings.ci
	@coverage report
	@coverage html