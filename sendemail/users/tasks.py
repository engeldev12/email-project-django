from django.contrib.auth.models import User
from django.template.loader import render_to_string

from celery import shared_task

from .email_messages import UserEmailMessage


@shared_task(bind=True)
def enviar_correo(self, user_id):

	user = User.objects.get(pk=user_id)

	user_email = UserEmailMessage(
		"Suscripción exitosa!",
		"Te haz suscrito exitosamente",
		"engeljavierpinto@gmail.com",
		[user.email]
	)

	template = render_to_string(
		template_name='users/email-success-suscription.html',
		context={'user': user}
	)
	user_email.attach_alternative(template, "text/html")

	user_email.send(fail_silently=False)