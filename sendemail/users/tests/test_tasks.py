from django.contrib.auth.models import User
from django.test import TestCase

from ..tasks import enviar_correo


class EnivarCorreoTaskTest(TestCase):


	def test_se_puede_enviar(self):

		user = User.objects.create_user(
			username='engel12',
			email="nerelply12@gmail.com",
			password='engel.engel'
		)

		task = enviar_correo.s(user.id).apply()

		self.assertEqual('SUCCESS', task.state)

	def test_con_un_usuario_que_no_existe_falla(self):
			
		user_id = 4

		task = enviar_correo.s(user_id).apply()		

		self.assertEqual('FAILURE', task.state)