from django.core import mail
from django.test import TestCase, override_settings
from django.urls import reverse


class UserCreateView(TestCase):

	def setUp(self):
		self.url = reverse('users:home')
		self.data = {
			'username': 'engel12',
			'email': 'engeljavi@gmail.com',
			'password1': 'sadddad.12',
			'password2': 'sadddad.12'
		}

	def test_ver_formulario_de_suscripcion(self):

		response = self.client.get(self.url)

		self.assertEqual(200, response.status_code)
		self.assertContains(response, 'Suscribete!')

	def test_sin_el_username_da_error(self):

		self.data['username'] = ""

		response = self.client.post(self.url, self.data)

		self.assertFormError(
				response,
				'form',
				'username',
				'Este campo es obligatorio.'
			)

	def test_sin_el_email_da_error(self):

		self.data['email'] = ''

		response = self.client.post(self.url, self.data)

		self.assertFormError(response,
				'form',
				'email',
				'Este campo es obligatorio.'
			)

	@override_settings(CELERY_TASK_ALWAYS_EAGER = True)
	def test_con_los_datos_validos_sea_crea_el_usuario_y_se_envia_un_correo(self):

		response = self.client.post(self.url, self.data)

		self.assertRedirects(response, reverse('users:success_suscription'))
		self.assertEqual(1, len(mail.outbox))
		self.assertEqual("Suscripción exitosa!", mail.outbox[0].subject)