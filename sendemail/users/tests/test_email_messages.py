from django.test import TestCase
from django.core import mail

from ..email_messages import UserEmailMessage


class UserEmailMessageTest(TestCase):

	def test_enviar_correo(self):

		user_email = UserEmailMessage(
			"Asunto",
			"Cuerpo",
			"engeljavierpinto@gmail.com",
			["otro.email@gmail.com"]
		)

		user_email.send(fail_silently=False)

		self.assertEqual(1, len(mail.outbox))
		self.assertEqual("Asunto", mail.outbox[0].subject)
		self.assertEqual("engeljavierpinto@gmail.com", mail.outbox[0].from_email)
