from django.test import TestCase
from ..forms import UserForm


class UserFormTest(TestCase):

	def setUp(self):
		self.data = {
			'username': 'engel12',
			'email': 'engel12dev@gmail.com',
			'password1': 'sadddad',
			'password2': 'sadddad'
		}

	def test_el_username_es_obligario(self):
		
		self.data['username'] = ""

		form = UserForm(self.data)

		self.assertFalse(form.is_valid())
		self.assertIn('Este campo es obligatorio.', form.errors['username'])

	def test_el_email_es_obligatorio(self):
		
		self.data['email'] = ""

		form = UserForm(self.data)

		self.assertFalse(form.is_valid())
		self.assertIn('Este campo es obligatorio.', form.errors['email'])