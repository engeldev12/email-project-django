from django.urls import reverse_lazy
from django.views import generic

from .forms import UserForm


class UserCreateView(generic.CreateView):
	form_class = UserForm
	template_name = 'users/home.html'
	success_url = reverse_lazy('users:success_suscription')

	def form_valid(self, form):
		form.send_email()
		return super().form_valid(form)


class UserSuccessSuscriptionView(generic.TemplateView):
	template_name = "users/success-suscription.html"
