from django.urls import path
from . import views


app_name = 'users'

urlpatterns = [
	path('', views.UserCreateView.as_view(), name="home"),
	path('suscripcion-exitosa', views.UserSuccessSuscriptionView.as_view(), name="success_suscription"),
]