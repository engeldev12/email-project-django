from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

from .tasks import enviar_correo


class UserForm(UserCreationForm):
	
	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.fields['email'].required = True
		self.initial = {
			"username": "",
			"email": ""
		}

	class Meta:
		model = User
		fields = (
			'username',
			'email',
			'password1',
			'password2',
		)

	def send_email(self):
		user = self.save()
		enviar_correo.delay(user.pk)