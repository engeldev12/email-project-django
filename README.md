# Email Project

[![pipeline status](https://gitlab.com/engeldev12/email-project-django/badges/master/pipeline.svg)](https://gitlab.com/engeldev12/email-project-django/-/commits/master)  [![coverage report](https://gitlab.com/engeldev12/email-project-django/badges/master/coverage.svg)](https://gitlab.com/engeldev12/email-project-django/-/commits/master)

Es un simple projecto con fines prácticos. Nunca he enviado un correo con django  
y quiero aprender a hacerlo.  

La app se tratará de suscripciones de usuarios. Cuando un usuario se suscribe,  
le llegará un correo informandole que se ha suscrito exitosamente.


# Ejecución de la app


## Dev

Primero crea tu entorno virtual, activalo e instala las dependencias con:  

```
	pip install -r requirements/dev.txt
```

Luego, descarga redis, instalalo y levanta el servidor para que celery se comunique con él.  

```
	cd redis/  
	make 32bit ó si tienes 64 -> make  
	cd src  
	./redis-serve
```  

Por último, activa celery:  

```
celery -A config.settings worker -l info

```  

Y ahora puedes correr tu app django.  

```
	make run
```  
